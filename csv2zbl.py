#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Reads ZBL file and stores CSV file."""

import sys
import logging
from data_io import zbl_io
from config import *
import csv

def yield_csv_rows(f, csv_separator="\t", cast_method=lambda x: x):
    """Loads CSV file and yields rows."""
    close = False
    if type(f)==str or type(f)==unicode:
        f = codecs.open(f, "r", "utf-8")
        close = True

    dmsoreader = csv.reader(f, delimiter=csv_separator)            
    for row in dmsoreader: 
        yield list(cast_method(e) for e in row)
        
    if close: 
        f.close()



if __name__ == "__main__":    

    reload(sys) 
    sys.setdefaultencoding('utf8')
    fin, fout = sys.stdin, sys.stdout
    sys.stdout = sys.stderr
    logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)


    print "The script reads CSV file from stdin and stores ZBL file to stdout."
    print "Args: separator [opt; default TAB], key column name [opt; default first column]"
    print "      empty value marker [opt; default ?], header [opt; default first row]"

    try: separator = "%s" % sys.argv[1]        
    except: separator = "\t"

    try: keycol = sys.argv[2]
    except: keycol = None

    try: empty_value = sys.argv[3]
    except: empty_value = EMPTY_VALUE

    try: header = sys.argv[4].split(',')
    except: header = None

    print "separator =",separator
    if header is not None:
        print "header =",header
    if keycol is not None:    
        print "keycol =",keycol
    print "empty_value =",empty_value
    
    for counter, row in enumerate( yield_csv_rows(fin, csv_separator=separator, cast_method=lambda x: x) ):
        if header is None:
            header = row
            print "header =",header
            if keycol is None:
                keycol = row[0]
                print "keycol =",keycol
        else:
            record = dict( (col, val) for col, val in zip(header, row) if val.strip()!=EMPTY_VALUE.strip() )
            record[ZBL_ID_FIELD] = record[keycol]
            zbl_io.write_zbl_record(fout, record)   
            fout.write("\n")    

    counter += 1
    print counter, "records stored"


