#!/usr/bin/env python
# -*- coding: utf-8 -*-

import inspect
from time import gmtime, strftime


class OutputDecorator:
    
    def __init__(self, out, prefix):
        """
        
        
            Args:
                out: Original output stream.
                prefix: A prefix string to be attached to each line. 
                        #F will be replaced with invoking method name.
                        #T with current time.
                        #S adjust prefix width by 10 characters 
                            e.g. #S#S#S forces prefix width to be at least 30.                        
        """
        self._out = out        
        self._prefix = prefix.replace("#S", "")
        self._hash_f = "#F" in prefix
        self._hash_t = "#T" in prefix
        self._adjust_prefix_size = self._adjust_prefix_size = prefix.count("#S")*10
        self._last_char = "\n"

    @staticmethod
    def astrlen(s, l=30):
        return s.ljust(l, " ")
        
    def write(self, txt):
        if len(txt)==0: return 
         
        #format prefix
        prefix = self._prefix
        if self._hash_f:               
            curframe = inspect.currentframe()
            calframe = inspect.getouterframes(curframe, 2)
            prefix = prefix.replace("#F", calframe[1][3])
        if self._hash_t:
            prefix = prefix.replace("#T", strftime("%Y-%m-%d %H:%M:%S", gmtime()))
        prefix = OutputDecorator.astrlen(prefix, self._adjust_prefix_size)
        
        #format output
        if self._last_char=="\n":
            self._out.write(prefix)
        self._out.write(txt.rstrip("\n").replace("\n", "\n%s" % prefix))        
        self._last_char = txt[-1]
        if self._last_char=="\n":
            self._out.write("\n")     
            
            
            