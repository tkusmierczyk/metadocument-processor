#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys

sys.path.append(r'../')

from processors.authors import *
from processors.fields import *
from processors.files import *
from processors.gensim import *
from processors.wdsim import *
from processors.graph import *
from processors.text import *
from processors.topics import *

from config import *



#List of supported commands
COMMANDS = [\
("add_field", add_field_command, "[field name] [field value]  adds to every record field of given name and value"),
("remove_field", remove_field_command, "[field name]  removes from every record field of given name"),
("copy_field", copy_field_command, "[src fieldname] [dst fieldname]  copies value of source field to destination field"),
("move_field", move_field_command, "[src fieldname] [dst fieldname]  moves value of source field to destination field"),
("merge_fields", merge_fields_command, "[list_of_src_fields_separated_with ,] [dst field] [separator_(optional)] merges values of source fields and puts result into dst_field"),
("must_have_fields", keep_records_with_fields_command, "[comma separated list of fields]  keeps only those records that have all fields in list_of_fields"),
("records_subset", keep_records_subset_command, "[a path to the file with one id per lines]  keeps only records with ids from the file"),
("keep_field", keep_field_command, '[field_name]  keeps only field of field_name in records'),
("extract_field", extract_field_command, '[field_name]  extracts value (just as a list of values) of field of field_name from records'),
("", None, None),
("rm_id", filter_id_cmd_name, "[id_list_file]  removes records of id contained in id_list_file (single id per line),"),
("keep_id", keep_id_cmd_name, "[id_list_file]  keeps records of id contained in id_list_file (single id per line),"),
("rm_duplicated_id", filter_id_duplicates_cmd_name, "  removes records with id duplicated"    ),
("append_file", append_file_cmd, "[append_file_path]  appends append_file_path file to source file"),
("copy_file", copy_file_cmd, "  copies file from source to destination"),
("", None, None),
("filter_text", filter_text_command, '[comma separated list of fields]  filters fields from list_of_fields  removes punctuation, strange symbols and lowers text.'),
("filter_to_ascii", filter_to_ascii_command, '[comma separated list of fields]  filters fields from list_of_fields: keeps only ascii chars.'),
("filter_regex", filter_fields_text_values_regex_command, "[comma separated list of fields] [regex] removes words that match regex from selected fields"),
("filter_min_word_count", word_count_filter_command, '[comma separated list of fields] [min_word_count (optional)] [dst_field_name_(optional)]  merges selected fields and removes words occurring <  min_word_count (results stored as additional field),'),
("stopwords",stopwords_text_command,'[stopword-list(short/long/standard/wiki) or file path] [comma separated list of fields] removes words from stopwords list in selected fields (embedded stopwords list are only lower case!)'),
("stemming", stemming_command, '[method] [comma separated list of fields]  stems fields from list_of_fields using given method (lancaster/porter/wordnet),'   ),
("ngrams", ngrams_command, '[n_value] [comma separated list of fields] [ngrams_separator_(optional)]  converts single words in fields into ngrams (connected with ngrams_separator),'),
("ngrams2", ngrams2_command, '[n_value] [comma separated list of fields] [ngrams_separator_(optional)]  converts single words in fields into ngrams (connected with ngrams_separator),'),
("mgrams", mgrams_command, '[max_n_value] [comma separated list of fields] [ngrams_separator_(optional)]  converts single words in fields into multi_ngrams (connected with ngrams_separator),' ),
("wrap", wrap_field_command, '[field name] converts list of whitespace separated words into dictionary: word->count'),
("unwrap", unwrap_field_command, '[field name] converts dictionary: word->count into list of whitespace separated words'),
("", None, None),
("gensim_dict", gensim_dict_command, '[comma separated list of fields] [filter_by_fields_(optional)] [gen_sim_dict_pickle_(optional)] [min word freq in corpora_(optional)]  builds and pickles gensim dictionary (token_>id),'),
("gensim_map", gensim_map_command, '[comma separated list of fields] [filter_by_fields_(optional)] [gen_sim_dict_pickle_(optional)] [dst_field_name_(optional)]  merges selected fields and maps them using gensim_dictionary (results stored as additional field),'),
("gensim_tfidf", gensim_tfidf_command, '[gen_sim_tfidf_model_pickle_(optional)] [src_field_name_(optional)]  builds and pickles gensim tfidf model'),
("gensim_tfidfmap", gensim_tfidfmap_command, '[gen_sim_tfidf_model_pickle_(optional)] [src_field_name_(optional)] [dst_field_name_(optional)]  calculates tfidf for src_field and stores into dst_field'),
("gensim_lda", gensim_lda_command, "[num_topics_(optional)] [id_to_weight_field_name_(optional)] [gensim_dict_pickle_path_(optional)] [gensim_semantic_pickle_path_(optional)]  [topics_log_path_(optional)]  builds lda model"),
("gensim_lsa", gensim_lsa_command, "[num_topics_(optional)] [id_to_weight_field_name_(optional)] [gensim_dict_pickle_path_(optional)] [gensim_semantic_pickle_path_(optional)]  [topics_log_path_(optional)]  builds lsa model"),
("gensim_semantic_map", gensim_semantic_map_command,"[id_to_weight_src_field_name_(optional)] [id_to_weight_dst_field_name_(optional)] [gensim_semantic_model_pickle_path_(optional)]  calculates weights using semantic_model"),
("wd_model", wd_model_command, "[id_to_count_src_field_name_(optional)] [words_docs_model_pickle_path_(optional)] builds words x docs model"),
("wd_map", wd_map_command, "[mode:tf-idf/tf-ent/wf-idf/...] [id_to_count_src_field_name_(optional)] [words_docs_model_pickle_path_(optional)] [id_to_weight_dst_field_name_(optional)] maps words' counts into words' weights using model"),
("", None, None),
("topic_tracking", topic_usage_plot_command, "[src-field-name with dictionary {topic:weight} (g2 by default)] [year field (py by default)] [1/0 if normalize or not (1 by default)] displays information about summarized weight of topics in time"),
("top_words", top_words_command, "[src-field-name with dictionary {word:weight} (g1 by default)] [k (default =5)] displays information about top-k words"),
("mean_similarity", one_leave_out_similarity_command, "[src-field-name with dictionary {word:weight} (g1 by default)] calculates measure similarity to all others documents"),
("", None, None),
("ci_graph", ci_graph, "extracts citations graph from source file"),
("dl_ci_graph", dl_ci_graph, "extracts citations graph from source file (connections are both ways),"),
("af_graph", af_graph, "extracts common_authorship graph from source file"),
("jn_graph", jn_graph, "extracts common_journal_number graph from source file"),
("extract_authors", extract_authors_cmd_name, '  keeps just authors information (fields: au, ai),'),
("filter_author_fingerprints", filter_author_fingerprints_cmd_name, '  removes from records empty af (only "_" values), fields')
]

#TODO
#(msc_model, "[msc-model-pickle-path (optional)] [msc-values-field-name (optional)] calculates msc-counts"),
#(msc_membership, "[msc-model-pickle-path (optional)] [msc-values-field-name (optional)] [dst-membership-field-name (optional)] [msc-codes-filter:0=all,1=ordinary,2=special (optional)] calculates msc-membership"),


###############################################################################


def _short_cmd_(cmd, cmd2method = {}):
    """Constructs short version of a command."""
    shortcmd = "".join(p[0] for p in cmd.split("_"))
    
    if shortcmd not in cmd2method: 
        return shortcmd
    
    suffix = 2
    while shortcmd+str(suffix) in cmd2method: suffix += 1
    return shortcmd+str(suffix)
      

def print_command_description(output, cmd2method, cmd, method, desc):
    shortcmd = _short_cmd_(cmd, cmd2method)
    cmd2method[cmd] = cmd2method[shortcmd] = method
    header = "  %s / %s  " % (cmd, shortcmd)
    output.write(header)
    l = len(header)
    for word in desc.split(" "):
        if l > 120:
            l = 20
            output.write("\n                  ")
        l += len(word)
        output.write(word)
        output.write(" ")    
    output.write("\n")
    

def build_cmd2method(commands, output=sys.stdout):
    """Constructs dictionary {command: method}."""
    cmd2method = {}
    for cmd, method, desc in commands:
        if len(cmd)<=0:
            output.write("  -----------------\n")
            continue        
        print_command_description(output, cmd2method, cmd, method, desc)
    return cmd2method
        
        
def print_help_(output=sys.stdout):
    print "MetaDocument-Processor (metadoc) is a script that allows for processing of (meta-)documents."
    print "Documents must be represented as ZBL-like (http://zbmath.org/) records [CSV converters are provided]."
    print "Stream of documents is read from standard input and printed to standard output."
    print "Usage: python",sys.argv[0],"command option1 option2 ... < input-stream > output-stream"
    print "Currently supported commands:"
    print "  command / short-abbreviation [options]  description"
    print "  ---------------------------------------------------" 
    build_cmd2method(COMMANDS, output)
                           
                           
                                  
