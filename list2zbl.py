#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Reads file and stores ZBL file where each line is a single document."""

import sys
import logging
from data_io import zbl_io
from config import *

if __name__ == "__main__":    

    reload(sys) 
    sys.setdefaultencoding('utf8')
    fin, fout = sys.stdin, sys.stdout
    sys.stdout = sys.stderr
    logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)

    print "Reads file from stdin and stores ZBL file to stdout where each line is a single document."

    try: id_prefix = sys.argv[1]
    except: id_prefix = "doc"

    try: field_name = sys.argv[2]
    except: field_name = "ti"

    print "Parameters: id_prefix =",id_prefix,", field_name =",field_name
    for i,line in enumerate(fin.xreadlines()):
        record = {ZBL_ID_FIELD: id_prefix+"%.6d"%i, field_name: line.strip()}
        zbl_io.write_zbl_record(fout, record)
        fout.write("\n")
    print i, "records stored"


