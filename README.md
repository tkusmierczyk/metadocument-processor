metadocument-processor
======================

metadocument-processor is a tool to process generalized (mostly textual) meta documents. 
Currently supported functionalities:
 - simple field edit (cp,mv,...) functionalities
 - text filtering, stemming, stopwords filtering
 - ngrams calculation
 - TF-IDF, LDA, LSA calculation
 - and more.

For full list try: 
```
 python metadoc.py
```

# Usage

Stream of documents is read from standard input and printed to standard output. Usage: 
```
 python metadoc.py command option1 option2 ... < input-stream > output-stream
```

For quick start see **QUICKSTART.txt**


# Internal format

Documents should be represented using (extended) Zentrallblatt MATH (zbl) format. 
Each document is represented as a record. 
Each record is composed of several text lines (in text file; whole collection can be stored in a single file). 
Each line start with two letter field code. 
First line in a record should start with ID ("an" field). 
Other fields are: *ab* - abstract/text, *ti* - title, *au* - authors, *ci* - citations etc. 
Sample document can be found in **sample_data/sample.zbl**

# Converters

The following converters to/from popular formats are provided:
 - csv2zbl.py
 - list2zbl.py
 - zbl2csv.py

# Commands

 -   add_field / af  [field name] [field value]  adds to every record field of given name and value 
 -   remove_field / rf  [field name]  removes from every record field of given name 
 -   copy_field / cf  [src fieldname] [dst fieldname]  copies value of source field to destination field 
 -   move_field / mf  [src fieldname] [dst fieldname]  moves value of source field to destination field 
 -   merge_fields / mf2  [list_of_src_fields_separated_with ,] [dst field] [separator_(optional)] merges values of source fields and puts    result into dst_field 
 -   must_have_fields / mhf  [comma separated list of fields]  keeps only those records that have all fields in list_of_fields 
 -   keep_field / kf  [field_name]  keeps only field of field_name in records 
 -   extract_field / ef  [field_name]  extracts value (just as a list of values) of field of field_name from records 
 -   rm_id / ri  [id_list_file]  removes records of id contained in id_list_file (single id per line), 
 -   keep_id / ki  [id_list_file]  keeps records of id contained in id_list_file (single id per line), 
 -   rm_duplicated_id / rdi    removes records with id duplicated 
 -   append_file / af2  [append_file_path]  appends append_file_path file to source file 
 -   copy_file / cf2    copies file from source to destination 
 -   filter_text / ft  [comma separated list of fields]  filters fields from list_of_fields  removes punctuation, strange symbols and lowers    text. 
 -   filter_to_ascii / fta  [comma separated list of fields]  filters fields from list_of_fields: keeps only ascii chars. 
 -   stopwords / s  [stopword-list(short/long/standard/wiki) or file path] [comma separated list of fields] removes words from stopwords    list in selected fields (embedded stopwords list are only lower case!) 
 -   stemming / s2  [method] [comma separated list of fields]  stems fields from list_of_fields using given method (lancaster/porter/wordnet), 
 -   ngrams / n  [n_value] [comma separated list of fields] [ngrams_separator_(optional)]  converts single words in fields into ngrams (connected    with ngrams_separator), 
 -   ngrams2 / n2  [n_value] [comma separated list of fields] [ngrams_separator_(optional)]  converts single words in fields into ngrams (connected    with ngrams_separator), 
 -   mgrams / m  [max_n_value] [comma separated list of fields] [ngrams_separator_(optional)]  converts single words in fields into multi_ngrams    (connected with ngrams_separator), 
 -   wrap / w  [field name] converts list of whitespace separated words into dictionary: word->count 
 -   unwrap / u  [field name] converts dictionary: word->count into list of whitespace separated words 
 -   gensim_dict / gd  [comma separated list of fields] [filter_by_fields_(optional)] [gen_sim_dict_pickle_(optional)] [min word freq    in corpora_(optional)]  builds and pickles gensim dictionary (token_>id), 
 -   gensim_map / gm  [comma separated list of fields] [filter_by_fields_(optional)] [gen_sim_dict_pickle_(optional)] [dst_field_name_(optional)] merges selected fields and maps them using gensim_dictionary (results stored as additional field), 
 -   gensim_tfidf / gt  [gen_sim_tfidf_model_pickle_(optional)] [src_field_name_(optional)]  builds and pickles gensim tfidf model 
 -   gensim_tfidfmap / gt2  [gen_sim_tfidf_model_pickle_(optional)] [src_field_name_(optional)] [dst_field_name_(optional)]  calculates    tfidf for src_field and stores into dst_field 
 -   gensim_lda / gl  [num_topics_(optional)] [id:weight_field_name_(optional)] [gensim_dict_pickle_path_(optional)] [gensim_semantic_pickle_path_(optional)] [topics_log_path_(optional)]  builds lda model 
 -   gensim_lsa / gl2  [num_topics_(optional)] [id:weight_field_name_(optional)] [gensim_dict_pickle_path_(optional)] [gensim_semantic_pickle_path_(optional)] [topics_log_path_(optional)]  builds lsa model 
 -   gensim_semantic_map / gsm  [id:weight_src_field_name_(optional)] [id:weight_dst_field_name_(optional)] [gensim_semantic_model_pickle_path_(optional)] calculates weights using semantic_model 
 -   wd_model / wm  [id:count_src_field_name_(optional)] [words_docs_model_pickle_path_(optional)] builds words x docs model 
 -   wd_map / wm2  [mode:tf-idf/tf-ent/wf-idf/...] [id:count_src_field_name_(optional)] [words_docs_model_pickle_path_(optional)]    [id:weight_dst_field_name_(optional)] maps words' counts into words' weights using model 
 -   topic_tracking / tt  [src-field-name with dictionary {topic:weight} (g2 by default)] [year field (py by default)] [1/0 if normalize or    not (1 by default)] displays information about summarized weight of topics in time 
 -   top_words / tw  [src-field-name with dictionary {word:weight} (g1 by default)] [k (default =5)] displays information about top-k words 
 -   mean_similarity / ms  [src-field-name with dictionary {word:weight} (g1 by default)] calculates measure similarity to all others documents 
 -   ci_graph / cg  extracts citations graph from source file 
 -   dl_ci_graph / dcg  extracts citations graph from source file (connections are both ways), 
 -   af_graph / ag  extracts common_authorship graph from source file 
 -   jn_graph / jg  extracts common_journal_number graph from source file 
 -   extract_authors / ea    keeps just authors information (fields: au, ai), 
 -   filter_author_fingerprints / faf    removes from records empty af (only "_" values), fields 


