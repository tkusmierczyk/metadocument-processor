#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Processes ZBL file by modifying every record according to rules."""

import sys
#import logging
import errno

from tools.aux import NullFile
from tools.aux import ArgsException

from ui.cmds import *
from ui.out_formatter import *


def main():

    reload(sys) 
    sys.setdefaultencoding('utf8')
    fin, fout = sys.stdin, sys.stdout
    sys.stdout = OutputDecorator(sys.stderr, "[%i][#T]#S#S#S" % os.getpid())
    sys.argv = ['stdin', 'stdout'] + sys.argv
    #logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)

    try: 
        cmd = sys.argv[3]        
    except:
        print "Error: Argument expected: command (what to do with records). Try -h/help for help."          
        #print_help_(sys.stdout)
        sys.exit(-1)
        
    cmd2method = build_cmd2method(COMMANDS, output=NullFile())
    if cmd.lower() == "-h" or cmd.lower()=="help":
        print_help_(sys.stdout)
        sys.exit(0)
    elif cmd not in cmd2method:
        print "Error: Unknown command! Try -h/help for help."
        #print_help_(sys.stdout) 
        sys.exit(-1)
    method = cmd2method[cmd]

    try:        
        script_start = time.clock()
        print "Starting execution for command:", cmd        
        method(fin, fout, sys)
        print "Command:"," ".join(sys.argv[3: ]),"executed in", (time.clock()-script_start),"sec"
    except IOError as e:
        if e.errno == errno.EPIPE: print "Pipe broken while processing. Finishing command",cmd,"..."
        else:   print "IOexception for command",cmd,":",e  
    except ArgsException as e:
        print "Arguments Error for command",cmd,":",e     


if __name__ == "__main__":    
    main()
