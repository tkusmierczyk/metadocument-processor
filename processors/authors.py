#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Processes ZBL stream by modifying authors-conneced data."""

def _extract_surname_name_(astr):
    try:
        parts = astr.split(',')        
        return [parts[0].strip(), parts[1].strip()]
    except:
        pass
    try:
        parts = astr.split('  ')        
        return [parts[0].strip(), parts[1].strip()]        
    except:
        pass
    try:
        parts = astr.split(' ')        
        return [parts[0].strip(), parts[1].strip()]        
    except:
        pass
    #print "[_extract_surname_name_] Warn:", astr
    return [astr, ""]


def _reformat_au_(austr):
    authors = []        
    #print "_reformat_au_", austr
    for author in austr.split(";"):    
        author = author.strip()
        if len(author) < 1:
            continue        
        parts = _extract_surname_name_(author)
        author_packed = zbl_io.pack_multivalue_field(parts, zbl_io.MULTIVAL_FIELD_SEPARATOR2);                
        authors.append(author_packed)        
    authors_packed = zbl_io.pack_multivalue_field(authors)
    return authors_packed


def _reformat_ai_(austr):
    authors = []        
    for author in austr.split(";"):
        author = author.strip()
        if len(author) < 1:
            continue                        
        authors.append(author)        
    authors_packed = zbl_io.pack_multivalue_field(authors)
    return authors_packed
        

def record_keep_authors(record):
    """Basing on records creates new record with only authorship information kept."""
    newrec = {}
    newrec[zbl_io.ZBL_ID_FIELD] = record[zbl_io.ZBL_ID_FIELD]
    if record.has_key('au'):
        newrec['au'] =  _reformat_au_( record['au'] )
    if record.has_key('ai'):
        newrec['ai'] =  _reformat_ai_( record['ai'] )
    return newrec


def keep_authors(fin, fout):
    """Copies all records from fin to fout. 
    
    
    Removes all fields apart from an, au, ai.
    Returns number of all copied records.
    """
    counter = 0
    for record in zbl_io.read_zbl_records(fin):
        zbl_io.write_zbl_record(fout, record_keep_authors(record))
        fout.write("\n")    
        counter = counter + 1 
    return counter
                   
      
def filter_af(fin, fout):
    """Copies records from fin to fout but also removes from records empty (only "-" values) af fields.
    
    Returns number of removed fields.
    """
    counter = 0
    for record in zbl_io.read_zbl_records(fin):
        if record.has_key("af"):
            af = zbl_io.unpack_multivalue_field(record["af"])
            empty = sum(1 for a in af if a == '-') == len(af)
            if empty:
                record.pop("af")
                counter = counter + 1
        zbl_io.write_zbl_record(fout, record)
        fout.write("\n")            
    return counter


def extract_authors_cmd_name(fin,fout, sys):
    print keep_authors(fin, fout), "records processed."


def filter_author_fingerprints_cmd_name(fin, fout, sys):
    print filter_af(fin, fout), "empty af fields removed."


