#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""ZBL files analysis with the use of topics weights vectors."""

import sys
import pickle
import logging
import nltk

sys.path.append(r'../')
from data_io import zbl_io
from gensim import extract_listpairs_fieldvalue
from doc_features.similarity import cosine 
from config import ZBL_ID_FIELD 

def parse_year(yearstr):
    if len(yearstr.strip())==2:
        return 1900+int(yearstr)
    return int(yearstr)
        

def extract_listpairs_fieldvalue(fieldvalue, val_cast = int, key_cast = int):
    """Takes packed-list-of-pairs-ZBL-field and returns list of pairs (key_cast(dict-key), val_cast(dict-value))."""
    return list((key_cast(k),val_cast(v)) for k,v in zbl_io.unpack_listpairs_field(fieldvalue))


def extract_topics_in_time(fin, src_field, year_field):

    year2topic2weight = {}
    for i,record in enumerate(zbl_io.read_zbl_records(fin)):                            
        if i%10000 == 0: logging.info("[extract_topics_in_time] "+str(i)+" documents processed...")
        if year_field not in record  or  src_field not in record: 
            continue
        try:
            year = parse_year(record[year_field])
        except:
            print "ERROR: failed to extract year value in record "+record
            continue

        topic_weight_pairs = extract_listpairs_fieldvalue(record[src_field], val_cast = float, key_cast = int)
        for topic,weight in topic_weight_pairs:
            topic2weight = year2topic2weight.setdefault(year, {})
            topic2weight[topic] = topic2weight.get(topic, 0.0) + weight

    return year2topic2weight


def topic_usage_plot_command(fin, fout, sys):

    try:
        src_field = sys.argv[4]
    except:
        src_field = 'g2' 
    try:
        year_field = sys.argv[5]
    except:
        year_field = 'py'
    try:
        normalize = (int(sys.argv[6])==1)
    except:
        normalize = 1
    print "src_field =",src_field,", year_field =",year_field," normalize =",normalize

    year2topic2weight = extract_topics_in_time(fin, src_field, year_field)

    years = list( year for year in sorted(year2topic2weight.keys()) )
    topics = set()
    for year,topic2weight in year2topic2weight.iteritems():
        topics.update(topic2weight.keys())

    print "    \t"+"\t".join(("%5d" % topic) for topic in topics)
    for year in years:
        topic2weight = year2topic2weight[year]
        if normalize:
            norm = sum(topic2weight.values())
        else:
            norm = 1.0
        print str(year)+"\t"+"\t".join( ("%.4f" % (topic2weight.get(topic, 0.0)/norm)) for topic in topics )


def top_words_command(fin, fout, sys):

    try:
        src_field = sys.argv[4]
    except:
        src_field = 'g1' 
    dst_field = "w1"
    dst_field2 = "w2"
    try:
        k = int(sys.argv[5])
    except:
        k = 5
   
    logging.info("[top_words_command] src_field=%s dst_field=%s dst_field2=%s k=%i" % \
                    (src_field, dst_field, dst_field2, k))

    for i, record in enumerate(zbl_io.read_zbl_records(fin)):                            
        if i%10000 == 0: logging.info("[top_words_command] "+str(i)+" records processed")

        word2wordstr = dict(extract_listpairs_fieldvalue(record["g_"], val_cast = str, key_cast = int)) \
                        if "g_" in record else dict()

        if src_field in record:
            word2weight = extract_listpairs_fieldvalue(record[src_field], val_cast = float, key_cast = int)
            word2weight = sorted(word2weight, key=lambda k: -k[1])
            word2weight = word2weight[:k]
            record[dst_field] = zbl_io.pack_listpairs_field(word2weight)
            word2weight = list((word2wordstr.get(word,word),weight) for word,weight in word2weight)
            record[dst_field2] = zbl_io.pack_listpairs_field(word2weight)
    
        zbl_io.write_zbl_record(fout, record)
        fout.write("\n") 


def one_leave_out_similarity_command(fin, fout, sys):

    try:
        src_field = sys.argv[4]
    except:
        src_field = 'g1' 
    dst_field = "sv"

    try:
        k = int(sys.argv[5])
    except:
        k = 5
   
    logging.info("[one_leave_out_similarity] src_field=%s dst_field=%s" % (src_field, dst_field))

    #load data
    records = list(record for record in zbl_io.read_zbl_records(fin))

    #one leave out mean similarity
    id2w = dict( (record[ZBL_ID_FIELD], dict(extract_listpairs_fieldvalue(record[src_field], val_cast=float, key_cast=int)))
                for record in records if src_field in record )

    id2sim = dict()
    for i,w in id2w.iteritems():
        simvalues = list(cosine(w,wp) for ip,wp in id2w.iteritems() if i!=ip)
        id2sim[i] = float(sum(simvalues)) / len(simvalues)
        
    #update data
    for record in records:
        idval = record[ZBL_ID_FIELD]
        if idval not in id2sim: continue
        record[dst_field] = id2sim[idval]
    
    #store data
    for record in records:
        zbl_io.write_zbl_record(fout, record)
        fout.write("\n") 
    
    
    


