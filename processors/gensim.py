#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Processes ZBL stream by modifying every record 
   according to rules using GENSIM."""

import sys
import pickle
import logging

sys.path.append(r'../')
from data_io import zbl_io
from config import *

from doc_features.semantic_gensim import *
from doc_features.semantic_gensim_readers import *

import tools
from tools.aux import validate_arguments_list
from tools.aux import ArgsException


def gensim_topics_store(semantic_model, flog, topn=300):
    """Writes to flog raport on topics of semantic_model."""
    for topic_no in xrange(semantic_model.num_topics):
        topic = str( semantic_model.print_topic(topic_no, topn) )
        flog.write(str(topic_no)+" =\t"+topic+"\n")


def extract_listpairs_fieldvalue(fieldvalue, val_cast = int, key_cast = int):
    """Takes packed-list-of-pairs-ZBL-field and returns list of pairs (key_cast(dict-key), val_cast(dict-value))."""
    return list((key_cast(k),val_cast(v)) for k,v in zbl_io.unpack_listpairs_field(fieldvalue))


extract_bag_of_ids      = lambda fieldvalue: extract_listpairs_fieldvalue(fieldvalue, val_cast = int, key_cast = int)
extract_bag_of_tfidf    = lambda fieldvalue: extract_listpairs_fieldvalue(fieldvalue, val_cast = float, key_cast = int)


def id_bags_generator(fin, src_field, value_extractor=extract_bag_of_ids):
    """Returns generator that generates gensim-bags-of-ids/tfidfs (read from src_field) from ZBL-file fin."""
    return ( value_extractor(record[src_field]) for record in zbl_io.read_zbl_records(fin) if src_field in record )


def gensim_mapfields_dict(record, fields, filter_by_fields, dictionary, dst_field, id2token={}, dbg_field_name = "g_"):
    """For record, that have filter_by_fields, fields are merged, 
       mapped with gensim dictionary and stored in dst-field."""
    has_filter_by_fields = sum(1 for field in filter_by_fields if field in record) == len(filter_by_fields)
    if has_filter_by_fields: 
        fields_list_of_words = reduce(lambda w1,w2: w1+w2, (record[field].split() 
                                                                        for field in fields if field in record) )            
        fields_words_ids = dictionary.doc2bow(fields_list_of_words)
        logging.debug("[gensim_mapfields_dict]"+str(fields_list_of_words)+" -> "+str(fields_words_ids))
        if len(fields_words_ids) > 0:
            record[dst_field] = zbl_io.pack_listpairs_field( fields_words_ids )
            record[dbg_field_name] = zbl_io.pack_listpairs_field( (idx,id2token.get(idx,'?')) 
                                                for idx,count in fields_words_ids ) #this-line is for debugging purposes
    return record    


def gensim_mapfields_dict_file(fin, fout, fields, filter_by_fields, dictionary, dst_field, dbg_field_name = "g_"):
    """For every records from ZBL-fin-stream that have filter_by_fields 
     fields are merged, mapped with gensim dictionary and stored in dst-field.

       Returns number of processed records."""
    logging.info("[gensim_mapfields_dict_file] filter_by_fields="+str(filter_by_fields)+\
    " fields="+str(fields)+" dictionary="+str(dictionary)+" fin="+str(fin)+" dst_field="+str(dst_field))
    id2token = dict( (idx,token) for idx,token in dictionary.iteritems() ) #this-line is for debugging purposes
    counter = 0
    for i,record in enumerate(zbl_io.read_zbl_records(fin)):                            
        if i%10000 == 0: logging.info("[gensim_mapfields_dict_file] "+str(i)+" records processed")
        record = gensim_mapfields_dict(record, fields, filter_by_fields, dictionary, dst_field, id2token, dbg_field_name)
        if dst_field in record:
            counter = counter + 1 
        zbl_io.write_zbl_record(fout, record)
        fout.write("\n")            
    return counter


def gensim_mapfield_model(fin, fout, model, src_field, dst_field,\
                          src_field_value_extractor=extract_bag_of_ids, \
                          dst_field_value_builder=zbl_io.pack_listpairs_field):
    """For every records from ZBL-fin-stream that have src_field its content 
    is interpreted as gensim-bag-of-ids(with weights/counts/values) 
    and transformed using model (results are stored into dst_field).

    Returns number of enriched records."""
    logging.info("[gensim_mapfield_model] src_field="+str(src_field)+\
     " model="+str(model)+" fin="+str(fin)+" dst_field="+str(dst_field)+\
     " src_field_value_extractor="+str(src_field_value_extractor)+\
     " dst_field_value_builder="+str(dst_field_value_builder))

    counter = 0
    for i,record in enumerate(zbl_io.read_zbl_records(fin)):                            
        if i%10000 == 0: logging.info("[gensim_mapfield_model] "+str(i)+" documents mapped...")
        if src_field in record:
            bag_of_ids = src_field_value_extractor(record[src_field])
            tfidf_values = model[bag_of_ids]
            record[dst_field] = dst_field_value_builder(tfidf_values)
            logging.debug("[gensim_mapfield_model]"+record[src_field]+" -> "+record[dst_field])
            counter = counter + 1    
        zbl_io.write_zbl_record(fout, record)
        fout.write("\n")
    return counter


###############################################################################
###############################################################################


def gensim_dict_command(fin, fout, sys):
    validate_arguments_list(sys.argv, 5)
    list_of_fields = sys.argv[4].split(',')
    try:
        filter_by_fields = sys.argv[5].split(',')
    except:
        filter_by_fields = [ZBL_ID_FIELD]
    try:
        dict_pickle = sys.argv[6]
    except:
        dict_pickle = str(TEMPDIR)+"/gensim_dict.pickle"
    try:
        min_word_freq_in_corpora = int(sys.argv[7])
    except:
        min_word_freq_in_corpora = 2      
    print "list of fields to be merged and converted=",list_of_fields   
    print "filter_by_fields=",filter_by_fields
    print "dict_pickle =",dict_pickle
    print "min_word_freq_in_corpora =", min_word_freq_in_corpora
    
    #Dictionary: #for already filtered data
    start = time.clock()
    dictionary = build_dictionary( zbl_generator_q(fin, list_of_fields, filter_by_fields), 
                                   min_freq=min_word_freq_in_corpora)
    print "Building token2id dictionary=",dictionary," in", time.clock()-start,"s -> ", str(dictionary.token2id)[:100],"..."
    print "Pickling dictionary into file",dict_pickle
    pickle.dump(dictionary, open(dict_pickle, 'wb'))
       

def gensim_map_command(fin, fout, sys):
    validate_arguments_list(sys.argv, 5)
    list_of_fields = sys.argv[4].split(',')
    try:
        filter_by_fields = sys.argv[5].split(',')
    except:
        filter_by_fields = [ZBL_ID_FIELD]
    try:
        dict_pickle = sys.argv[6]
    except:
        dict_pickle = str(TEMPDIR)+"/gensim_dict.pickle"
    try:
        dst_field = sys.argv[7]
    except:
        dst_field = 'g0' 
    print "list of fields to be merged and converted=",list_of_fields   
    print "filter_by_fields=",filter_by_fields
    print "dict_pickle =",dict_pickle
    print "dst_field =",dst_field 
    
    print "Loading dictionary from file",dict_pickle
    dictionary = pickle.load( open(dict_pickle) )
    print "Loaded token2id dictionary=",dictionary," -> ", str(dictionary.token2id)[:100],"..."
    num_enriched_records = gensim_mapfields_dict_file(fin, fout, list_of_fields, filter_by_fields, dictionary, dst_field)
    print num_enriched_records, "records enriched..."
    
 

        
def gensim_tfidf_command(fin, fout, sys):
    try:
        tfidf_pickle = sys.argv[4]
    except:
        tfidf_pickle = str(TEMPDIR)+"/gensim_tfidf_model.pickle"
    try:
        src_field = sys.argv[5]
    except:
        src_field = 'g0'    
    print "tfidf_pickle =", tfidf_pickle
    print "src_field =", src_field         
    print "Building TFIDF model..."
    start = time.clock()
    tfidf_model = build_tfidf_model( id_bags_generator(fin, src_field) )
    print "Building TFIDF model=",tfidf_model," in", time.clock()-start,"s"
    print "Pickling dictionary into file",tfidf_pickle
    pickle.dump(tfidf_model, open(tfidf_pickle, 'wb'))


def gensim_tfidfmap_command(fin, fout, sys):
    try:
        tfidf_pickle = sys.argv[4]
    except:
        tfidf_pickle = str(TEMPDIR)+"/gensim_tfidf_model.pickle"
    try:
        src_field = sys.argv[5]
    except:
        src_field = 'g0'         
    try:
        dst_field = sys.argv[6]
    except:
        dst_field = 'g1' 
    print "tfidf_pickle =", tfidf_pickle
    print "src_field =", src_field         
    print "dst_field =", dst_field         
    
    print "Loading TFIDF model from file",tfidf_pickle
    tfidf_model = pickle.load( open(tfidf_pickle) )
    print "Loaded TFIDF model=",tfidf_model
    num_enriched_records = gensim_mapfield_model(fin, fout, tfidf_model, src_field, dst_field)            
    print num_enriched_records, "records enriched..."


def gensim_lda_command(fin, fout, sys): 
    try:
        num_topics = int(sys.argv[4])
    except:
        num_topics = 300
    try:
        src_field = sys.argv[5]
    except:
        src_field = 'g1'
    try:
        dict_pickle = sys.argv[6]
    except:
        dict_pickle = str(TEMPDIR)+"/gensim_dict.pickle"
    try:
        semantic_model_pickle = sys.argv[7]
    except:
        semantic_model_pickle = str(TEMPDIR)+"/gensim_semantic_model.pickle"    
    try:
        topics_log_path = sys.argv[8]
    except:
        topics_log_path = str(TEMPDIR)+"/gensim_semantic_model_topics.txt"
    print "num_topics =",num_topics
    print "src_field =",src_field
    print "dict_pickle =",dict_pickle
    print "semantic_model_pickle =",semantic_model_pickle
    print "topics_log_path =", topics_log_path
    
    print "Loading dictionary from file",dict_pickle
    dictionary = pickle.load( open(dict_pickle) )
    print "Building semantic model..."   
    start = time.clock()
    bag_of_ids_vals_generator = id_bags_generator(fin, src_field, value_extractor=extract_bag_of_tfidf)
    print "Building LDA-semantic model..."   
    semantic_model = build_lda_model(bag_of_ids_vals_generator, dictionary, num_topics)
    print "Storing topics into",topics_log_path 
    gensim_topics_store(semantic_model, open(topics_log_path, "w"), topn=100)        
    print "Building semantic model=",semantic_model," in", time.clock()-start,"s"
    print "Pickling model into file",semantic_model_pickle
    pickle.dump(semantic_model, open(semantic_model_pickle, 'wb'))        


def gensim_lsa_command(fin, fout, sys):
    try:
        num_topics = int(sys.argv[4])
    except:
        num_topics = 300
    try:
        src_field = sys.argv[5]
    except:
        src_field = 'g1'
    try:
        dict_pickle = sys.argv[6]
    except:
        dict_pickle = str(TEMPDIR)+"/gensim_dict.pickle"
    try:
        semantic_model_pickle = sys.argv[7]
    except:
        semantic_model_pickle = str(TEMPDIR)+"/gensim_semantic_model.pickle"    
    try:
        topics_log_path = sys.argv[8]
    except:
        topics_log_path = str(TEMPDIR)+"/gensim_semantic_model_topics.txt"
    print "num_topics =",num_topics
    print "src_field =",src_field
    print "dict_pickle =",dict_pickle
    print "semantic_model_pickle =",semantic_model_pickle
    print "topics_log_path =", topics_log_path
    
    print "Loading dictionary from file",dict_pickle
    dictionary = pickle.load( open(dict_pickle) )
    print "Building semantic model..."   
    start = time.clock()
    bag_of_ids_vals_generator = id_bags_generator(fin, src_field, value_extractor=extract_bag_of_tfidf)
    print "Building LSA-semantic model..."   
    semantic_model = build_lsi_model(bag_of_ids_vals_generator, dictionary, num_topics)
    print "Storing topics into",topics_log_path 
    gensim_topics_store(semantic_model, open(topics_log_path, "w"), topn=100)        
    print "Building semantic model=",semantic_model," in", time.clock()-start,"s"
    print "Pickling model into file",semantic_model_pickle
    pickle.dump(semantic_model, open(semantic_model_pickle, 'wb'))        


def gensim_semantic_map_command(fin, fout, sys):
    try:
        src_field = sys.argv[4]
    except:
        src_field = 'g1'
    try:
        dst_field = sys.argv[5]
    except:
        dst_field = 'g2'
    try:
        semantic_model_pickle = sys.argv[6]
    except:
        semantic_model_pickle = str(TEMPDIR)+"/gensim_semantic_model.pickle"
    print "src_field =",src_field
    print "dst_field =",dst_field
    print "semantic_model_pickle =",semantic_model_pickle
    
    print "Loading semantic model from file",semantic_model_pickle
    semantic_model = pickle.load(open(semantic_model_pickle))
    print "Loaded semantic model=",semantic_model
    num_enriched_records = gensim_mapfield_model(fin, fout, semantic_model, src_field, dst_field, \
                         src_field_value_extractor=extract_bag_of_tfidf, \
                         dst_field_value_builder=zbl_io.pack_listpairs_field)    
    print num_enriched_records, "records enriched..."






