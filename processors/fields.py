#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Processes ZBL stream by modifying every record 
   according to rules on fields' content."""

import sys
import pickle
import logging

sys.path.append(r'../')
from data_io import zbl_io

import tools
from tools.text_to_words import words_filter
from tools.stop_words_list import *
from tools.aux import validate_arguments_list


def add_field(fin, fout, add_field_name, add_field_value):
    """To every record from fin adds field (add_field_value:add_field_name) and stores record to fout."""
    for record in zbl_io.read_zbl_records(fin):
        record[add_field_name] = add_field_value            
        zbl_io.write_zbl_record(fout, record)
        fout.write("\n")


def add_field_command(fin, fout, sys):
    validate_arguments_list(sys.argv, 6)
    add_field_name = sys.argv[4]
    add_field_value = sys.argv[5] 
    print "add_field_name=",add_field_name  
    print "add_field_value=",add_field_value
    add_field(fin, fout, add_field_name, add_field_value)


def remove_field(fin, fout, rm_field_name):
    """In every record from fin removes field rm_field_name and stores record to fout."""
    for record in zbl_io.read_zbl_records(fin):
        if rm_field_name in record: record.pop(rm_field_name)
        zbl_io.write_zbl_record(fout, record)
        fout.write("\n")


def remove_field_command(fin, fout, sys):
    validate_arguments_list(sys.argv, 5)
    rm_field_name = sys.argv[4]
    print "rm_field_name=",rm_field_name  
    remove_field(fin, fout, rm_field_name)

        
def copy_field(fin, fout, src_field, dst_field):
    """In every record from fin copies field src_field to field dst_field and stores record to fout."""
    for record in zbl_io.read_zbl_records(fin):
        if record.has_key(src_field):            
            record[dst_field] = record[src_field]
        zbl_io.write_zbl_record(fout, record)
        fout.write("\n")


def copy_field_command(fin, fout, sys):
    validate_arguments_list(sys.argv, 6)
    src_field = sys.argv[4]
    dst_field = sys.argv[5]
    print "src_field=",src_field   
    print "dst_field=",dst_field
    copy_field(fin, fout, src_field, dst_field)


def mv_field(fin, fout, src_field, dst_field):
    """In every record from fin moves field src_field to field dst_field and stores record to fout."""
    for record in zbl_io.read_zbl_records(fin):
        if record.has_key(src_field):            
            record[dst_field] = record[src_field]
            record.pop(src_field)
        zbl_io.write_zbl_record(fout, record)
        fout.write("\n")


def move_field_command(fin, fout, sys):
    validate_arguments_list(sys.argv, 6)
    src_field = sys.argv[4]
    dst_field = sys.argv[5]
    print "src_field=",src_field   
    print "dst_field=",dst_field
    mv_field(fin, fout, src_field, dst_field)

        
def merge_fields(fin, fout, src_fields, dst_field, separator = " "):
    """In every record from fin merges fields from src_field to field dst_field and stores record to fout."""
    for record in zbl_io.read_zbl_records(fin):
        try:
            dst_val = reduce(lambda a,b: a+separator+b, (record[src_field] for src_field in src_fields if src_field in record) )
            record[dst_field] = dst_val
        except:
            print "[merge_fields] Failed merging in record an=", record[zbl_io.ZBL_ID_FIELD]
        zbl_io.write_zbl_record(fout, record)
        fout.write("\n")


def merge_fields_command(fin, fout, sys):
    validate_arguments_list(sys.argv, 6)
    list_of_fields = sys.argv[4].split(',')
    dst_field = sys.argv[5]
    try:
        separator = sys.argv[6]
    except:
        separator = ' '
    print "src_fields=",list_of_fields
    print "dst_field=",dst_field
    print "separator=",separator
    merge_fields(fin, fout, list_of_fields, dst_field, separator)


def keep_field(fin, fout, field_name):
    """Copies records from fin to fout but keeping only id and field of field_name.    
    
    Returns number of found fields.
    """
    counter = 0
    for record in zbl_io.read_zbl_records(fin):
        newrec = {}
        newrec[zbl_io.ZBL_ID_FIELD] = record[zbl_io.ZBL_ID_FIELD]
        if record.has_key(field_name):
            newrec[field_name] = record[field_name]            
            counter = counter + 1
        zbl_io.write_zbl_record(fout, newrec)
        fout.write("\n")            
    return counter    


def keep_field_command(fin, fout, sys):
    validate_arguments_list(sys.argv, 5)
    field_name = sys.argv[4]
    print keep_field(fin, fout, field_name), "records copied."


def extract_field(fin, fout, field_name):
    """Extracts to fout value of a field of field_name.    
    
    Returns number of found fields.
    """
    counter = 0
    for record in zbl_io.read_zbl_records(fin):
        if record.has_key(field_name):
            fout.write(str(record[field_name]))
            fout.write("\n")            
            counter = counter + 1
    return counter    


def extract_field_command(fin, fout, sys):
    validate_arguments_list(sys.argv, 5)
    field_name = sys.argv[4]
    print extract_field(fin, fout, field_name), "values extracted."    


def _has_all_fields_(record, must_have_fields):
    """Returns True iff record contains all fields."""
    for field in must_have_fields:
        if not field in record:
            return False
    return True


def keep_records_with_fields(fin, fout, must_have_fields):
    """Copies records from fin to fout. 
    
    Keeps only these records that have all fields from must_have_fields list.
    """
    kept_counter = 0
    for i,record in enumerate(zbl_io.read_zbl_records(fin)):
        if i%10000 == 0: print "[keep_records]", i,"processed", kept_counter, "kept"
        if _has_all_fields_(record, must_have_fields):
            zbl_io.write_zbl_record(fout, record)
            fout.write("\n")  
            kept_counter = kept_counter + 1  
    return kept_counter


def keep_records_with_fields_command(fin, fout, sys):
    validate_arguments_list(sys.argv, 5)
    list_of_fields = sys.argv[4].split(',')    
    print "must_have_fields=",list_of_fields   
    kept = keep_records_with_fields(fin, fout, list_of_fields)
    print kept," records have been kept." 


def keep_records_subset(fin, fout, subset_ids):
    """Copies records from fin to fout. 
    
    Keeps only these records that are on the list (have ids).
    """
    subset_ids = set(subset_ids)
    kept_counter = 0
    for i,record in enumerate(zbl_io.read_zbl_records(fin)):
        if i%10000 == 0: print "[keep_records_subset]", i,"processed", kept_counter, "kept"
        if record[zbl_io.ZBL_ID_FIELD] in subset_ids:
            zbl_io.write_zbl_record(fout, record)
            fout.write("\n")  
            kept_counter = kept_counter + 1  
    print "[keep_records_subset]", (i+1),"processed", kept_counter, "kept"            
    return kept_counter


def keep_records_subset_command(fin, fout, sys):
    validate_arguments_list(sys.argv, 5)    
    subset_ids = [i.strip() for i in open(sys.argv[4]).xreadlines() 
                                if i.strip()!="" and (len(i)>0 and i[0]!="#")]   
    print "subset of record ids (",len(subset_ids),"entries )=",subset_ids[:50]
    kept = keep_records_subset(fin, fout, subset_ids)
    print kept," records have been kept." 



