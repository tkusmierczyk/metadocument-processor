#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Processes ZBL stream and extracts graphs."""

from graphs.zbl_extract_graph import *

def ci_graph(fin, fout, sys):
    print "extracting citations graph"
    extracted = extract_citations_graph_file(fin, fout)
    print extracted," lines extracted"


def dl_ci_graph(fin, fout, sys):
    print "extracting double-linked citations graph"
    extracted = extract_citations_doublelinked_graph_file(fin, fout)
    print extracted," lines extracted"


def af_graph(fin, fout, sys):
    print "extracting authorship graph"
    extracted = extract_fv_graph_file(fin, fout, "af", "-")
    print extracted," lines extracted"


def jn_graph(fin, fout, sys):
    print "extracting common-journal-number graph"
    extracted = extract_fv_graph_file(fin, fout, "jn", "-")
    print extracted," lines extracted"        


###############################################################################

    
