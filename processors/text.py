#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""ZBL files text processing."""

import sys
import pickle
import logging
import nltk
import string
import re

sys.path.append(r'../')
from data_io import zbl_io
from doc_features.ngrams import *
from doc_features.lwgw_weighting import *

from tools.text_to_words import words_filter
from tools.stop_words_list import *
from tools.aux import validate_arguments_list
from tools.aux import ArgsException
from tools.text_to_words import *




def filter_fields_text_values(fin, fout, list_of_fields, \
                                text_filter = text_filter, word_predicate = default_word_predicate):
    """Copies records from fin to fout and for fields on list_of_fields filters its' values."""
    for record in zbl_io.read_zbl_records(fin):
        for field in list_of_fields:   
            if record.has_key(field):         
                try:   
                    record[field] = words_filter(text_filter(record[field]), word_predicate)
                except:
                    #logging.warn("Removing field in an="+str(record[zbl_io.ZBL_ID_FIELD])+\
                    #    " (is field empty?):"+field+" = "+record[field])
                    #record.pop(field) 
                    pass            
        zbl_io.write_zbl_record(fout, record)
        fout.write("\n")


def filter_text_command(fin, fout, sys):
    validate_arguments_list(sys.argv, 5)
    list_of_fields = sys.argv[4].split(',')
    print "list of fields to be filtered = ", list_of_fields        
    filter_fields_text_values(fin, fout, list_of_fields, text_filter_lower, default_word_predicate)


def filter_fields_text_values_regex(fin, fout, list_of_fields, regex):
    """Copies records from fin to fout and for fields on list_of_fields removes words matchin regex."""
    pattern = re.compile(regex)
    for record in zbl_io.read_zbl_records(fin):
        for field in list_of_fields:   
            if record.has_key(field):         
                try:   
                    record[field] = " ".join(word for word in record[field].split(" ") if not pattern.match(word))    
                except:
                    #logging.warn("Removing field in an="+str(record[zbl_io.ZBL_ID_FIELD])+\
                    #    " (is field empty?):"+field+" = "+record[field])
                    #record.pop(field) 
                    pass            
        zbl_io.write_zbl_record(fout, record)
        fout.write("\n")


def filter_fields_text_values_regex_command(fin, fout, sys):
    validate_arguments_list(sys.argv, 5)
    list_of_fields = sys.argv[4].split(',')
    regex = sys.argv[5]    
    print "list of fields to be filtered = ", list_of_fields        
    print "regex = ", regex    
    filter_fields_text_values_regex(fin, fout, list_of_fields, regex)
    

def filter_fields_to_ascii(fin, fout, list_of_fields):
    """Copies records from fin to fout and for fields on list_of_fields filters its' values."""
    printable = set(string.printable)
    for record in zbl_io.read_zbl_records(fin):
        for field in list_of_fields:   
            if record.has_key(field):         
                try:   
                    record[field] = filter(lambda x: x in printable, record[field])
                except:
                    #logging.warn("Removing field in an="+str(record[zbl_io.ZBL_ID_FIELD])+\
                    #    " (is field empty?):"+field+" = "+record[field])
                    #record.pop(field)   
                    pass          
        zbl_io.write_zbl_record(fout, record)
        fout.write("\n")



def filter_to_ascii_command(fin, fout, sys):
    validate_arguments_list(sys.argv, 5)
    list_of_fields = sys.argv[4].split(',')
    print "list of fields to be filtered = ", list_of_fields        
    filter_fields_to_ascii(fin, fout, list_of_fields)


def stopwords_fields_text_values(fin, fout, list_of_fields, stopwords = set()):
    """Copies records from fin to fout and for fields on list_of_fields filters its' values."""
    stopwords = set(stopwords)
    for record in zbl_io.read_zbl_records(fin):
        for field in list_of_fields:   
            if record.has_key(field):         
                try:
                    words = record[field].split()
                    record[field] = " ".join(w for w in words if w not in stopwords)
                except:
                    logging.warn("Removing field in an="+str(record[zbl_io.ZBL_ID_FIELD])+\
                        " (is field empty?):"+field+" = "+record[field])
                    record.pop(field)             
        zbl_io.write_zbl_record(fout, record)
        fout.write("\n")


def stopwords_text_command(fin, fout, sys):
    validate_arguments_list(sys.argv, 6)
    stopwords_path = sys.argv[4].strip()
    if stopwords_path=="long": stopwords = LONG_STOP_WORDS_LIST
    elif stopwords_path=="standard": stopwords = STOP_WORDS_LIST
    elif stopwords_path=="short": stopwords = SHORT_STOP_WORDS_LIST
    elif stopwords_path=="wiki": stopwords = WIKI_STOP_WORDS_LIST
    else:
        try: stopwords = list(word.strip() for word in open(stopwords_path).readlines())
        except: raise ArgsException("Bad stopwords list! Try: long/standard/short/wiki or give file path")   
    list_of_fields = sys.argv[5].split(',')

    print "list of fields to be filtered = ", list_of_fields        
    print len(stopwords),"stopwords = ",str(stopwords[:20])[:50],"..."

    stopwords_fields_text_values(fin, fout, list_of_fields, stopwords)


def stemming_command(fin, fout, sys):    
    validate_arguments_list(sys.argv, 6)
    method = sys.argv[4]
    list_of_fields = sys.argv[5].split(',')
    print "List of fields to be filtered = ", list_of_fields
    print "Stemming method = ", method
    if method == 'lancaster':
        stemmer = nltk.stem.LancasterStemmer()
    elif method == 'porter': 
        stemmer = nltk.stem.PorterStemmer()    
    elif method == 'wordnet':
        stemmer = nltk.stem.WordNetStemmer()
        stemmer.stem = stemmer.lemmatize    
    else:
        raise  ArgsException("Bad stemming method (try: lancaster/porter/wordnet)!")
    line_stemmer = lambda line: reduce(lambda w1,w2: w1+' '+w2, (stemmer.stem(w) for w in line.split()) )
    filter_fields_text_values(fin, fout, list_of_fields, line_stemmer, default_word_predicate)
    #filter_fields_text_values(fin, fout, list_of_fields, line_stemmer, lambda x: True)


def mgrams_command(fin, fout, sys): 
    validate_arguments_list(sys.argv, 6)
    n = int(sys.argv[4])
    list_of_fields = sys.argv[5].split(',')
    try:
        ngram_separator = sys.argv[6]
    except:        
        ngram_separator = '-'
    print "List of fields to be processed = ", list_of_fields
    print "Max-n-value = ", n
    print "ngram-separator = <", ngram_separator,">"    
    build_mgrams_file(fin, fout, list_of_fields, n+1, ngram_separator)    


def ngrams_command(fin, fout, sys):
    validate_arguments_list(sys.argv, 6)
    n = int(sys.argv[4])
    list_of_fields = sys.argv[5].split(',')
    try:
        ngram_separator = sys.argv[6]
    except:        
        ngram_separator = '-'
    keep_endings = False   
    print "List of fields to be processed = ", list_of_fields
    print "N-value = ", n
    print "ngram-separator = <", ngram_separator,">"
    print "keep-endings =",keep_endings
    build_ngrams_file(fin, fout, list_of_fields, n, ngram_separator, keep_endings)


def ngrams2_command(fin, fout, sys):
    validate_arguments_list(sys.argv, 6)
    n = int(sys.argv[4])
    list_of_fields = sys.argv[5].split(',')
    try:
        ngram_separator = sys.argv[6]
    except:        
        ngram_separator = '-'
    keep_endings = True
    print "List of fields to be processed = ", list_of_fields
    print "N-value = ", n
    print "ngram-separator = <", ngram_separator,">"
    print "keep-endings =",keep_endings
    build_ngrams_file(fin, fout, list_of_fields, n, ngram_separator, keep_endings)



def wrap_field(fin, fout, field):
    for record in zbl_io.read_zbl_records(fin):
        if field in record:
            word2count = {}
            for word in record[field].split(" "):
                word2count[word] = word2count.get(word, 0) + 1
            record[field] = zbl_io.pack_dictionary_field(word2count)
        zbl_io.write_zbl_record(fout, record)
        fout.write("\n")


def wrap_field_command(fin, fout, sys):
    validate_arguments_list(sys.argv, 5)
    field = sys.argv[4].strip()
    wrap_field(fin, fout, field)


def unwrap_field(fin, fout, field):
    for record in zbl_io.read_zbl_records(fin):
        if field in record:
            word2count = zbl_io.unpack_dictionary_field(record[field])
            record[field] = " ".join(" ".join(word for i in xrange(int(count)))\
                                   for word, count in word2count.iteritems())                
        zbl_io.write_zbl_record(fout, record)
        fout.write("\n")
    

def unwrap_field_command(fin, fout, sys):
    validate_arguments_list(sys.argv, 5)
    field = sys.argv[4].strip()
    unwrap_field(fin, fout, field) 


def count_words(records, list_of_fields):
    word2count = dict()  
    for record in records:
        for field in list_of_fields:
            for word in record.get(field,[]).split(" "):
                word = word.strip()
                if word=="": continue
                word2count[word] = word2count.get(word, 0) + 1
    return word2count


def filter_words_with_counts(records, list_of_fields, word2count, min_count, dst_field):
    for record in records:
        dst = []
        for field in list_of_fields:
            for word in record.get(field,[]).split(" "):
                word = word.strip()
                if word=="" or word2count.get(word, 0)<min_count: continue
                dst.append(word)
        record[dst_field] = " ".join(dst)
    return records


def word_count_filter_command(fin, fout, sys):
    validate_arguments_list(sys.argv, 5)
    list_of_fields = sys.argv[4].split(',')
    try:
        min_count = int(sys.argv[5])
    except:
        min_count = 2
    try:
        dst_field = sys.argv[6]
    except:
        dst_field = 'tt' 
    print "list of fields to be merged and filtered=", list_of_fields   
    print "min word count=", min_count
    print "dst_field =", dst_field 
    
    #load
    records = list(zbl_io.read_zbl_records(fin))
    
    #count
    word2count = count_words(records, list_of_fields)
    #print "word2count =", word2count
    wc = sorted(list(word2count.iteritems()), key=lambda (w,c): -c)
    print "word2count =", wc[ :20], "...", wc[-20: ]
          
    #filter          
    records = filter_words_with_counts(records, list_of_fields, word2count, min_count, dst_field)
           
    #save     
    for record in records:      
        zbl_io.write_zbl_record(fout, record)
        fout.write("\n")
    

