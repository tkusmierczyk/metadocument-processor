#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Processes ZBL stream by modifying every record 
   according to rules using simple semantic models."""

import sys
import pickle
import logging

sys.path.append(r'../')
from data_io import zbl_io
from config import *

from doc_features.lwgw_weighting import *



def wd_model_command(fin, fout, sys):
    try:
        src_field = sys.argv[4]
    except:
        src_field = 'g0'
    try:
        model_pickle = sys.argv[5]
    except:
        model_pickle = str(TEMPDIR)+"/wd_model.pickle"
    print "src_field=",src_field
    print "model_pickle=",model_pickle
    print "Building Words-Docs model"
    wordsmodel = build_wordsmodel(fin, fout, src_field)
    wordsmodel.report()
    print "Pickling into", model_pickle
    pickle.dump(wordsmodel, open(model_pickle, 'wb'))


def wd_map_command(fin, fout, sys):
    try:
        mode = sys.argv[4]
    except:
        mode = "tf-idf"
    try:
        src_field = sys.argv[5]
    except:
        src_field = 'g0'
    try:
        model_pickle = sys.argv[6]
    except:
        model_pickle = str(TEMPDIR)+"/wd_model.pickle"
    try:
        dst_field = sys.argv[7]
    except:
        dst_field = 'g1' 
    print "mode=",mode
    lw = mode.split('-')[0]
    gw = mode.split('-')[1]
    if lw == "tf":
        localweigting = tf
    elif lw == "wf":
        localweigting = wf
    else:
        print "UNKNOWN LOCAL WEIGHTING METHOD!"
        sys.exit(-2)
    if gw == "idf":
        globalweigting = idf
    elif gw == "ent":
        globalweigting = ent
    else:
        print "UNKNOWN GLOBAL WEIGHTING METHOD!"
        sys.exit(-2)
    print "localweigting=",localweigting
    print "globalweigting=",globalweigting
    print "src_field=",src_field
    print "model_pickle=",model_pickle
    print "dst_field=",dst_field      
    print "Loading pickled model from", model_pickle
    wordsmodel = pickle.load(open(model_pickle))
    wordsmodel.report()
    print "Mapping src_field into dst_field..."
    enriched = map_wordsmodel(fin, fout, wordsmodel, src_field, dst_field, localweigting, globalweigting)
    print enriched,"records enriched."    


def wd_model_command(fin, fout, sys):
    try:
        src_field = sys.argv[4]
    except:
        src_field = 'g0'
    try:
        model_pickle = sys.argv[5]
    except:
        model_pickle = str(TEMPDIR)+"/wd_model.pickle"
    print "src_field=",src_field
    print "model_pickle=",model_pickle
    print "Building Words-Docs model"
    wordsmodel = build_wordsmodel(fin, fout, src_field)
    wordsmodel.report()
    print "Pickling into", model_pickle
    pickle.dump(wordsmodel, open(model_pickle, 'wb'))


def wd_map_command(fin, fout, sys):
    try:
        mode = sys.argv[4]
    except:
        mode = "tf-idf"
    try:
        src_field = sys.argv[5]
    except:
        src_field = 'g0'
    try:
        model_pickle = sys.argv[6]
    except:
        model_pickle = str(TEMPDIR)+"/wd_model.pickle"
    try:
        dst_field = sys.argv[7]
    except:
        dst_field = 'g1' 
    print "mode=",mode
    lw = mode.split('-')[0]
    gw = mode.split('-')[1]
    if lw == "tf":
        localweigting = tf
    elif lw == "wf":
        localweigting = wf
    else:
        print "UNKNOWN LOCAL WEIGHTING METHOD!"
        sys.exit(-2)
    if gw == "idf":
        globalweigting = idf
    elif gw == "ent":
        globalweigting = ent
    else:
        print "UNKNOWN GLOBAL WEIGHTING METHOD!"
        sys.exit(-2)
    print "localweigting=",localweigting
    print "globalweigting=",globalweigting
    print "src_field=",src_field
    print "model_pickle=",model_pickle
    print "dst_field=",dst_field      
    print "Loading pickled model from", model_pickle
    wordsmodel = pickle.load(open(model_pickle))
    wordsmodel.report()
    print "Mapping src_field into dst_field..."
    enriched = map_wordsmodel(fin, fout, wordsmodel, src_field, dst_field, localweigting, globalweigting)
    print enriched,"records enriched."           



