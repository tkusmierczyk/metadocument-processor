#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""ZBL files general processing."""

import sys
import pickle
import logging

sys.path.append(r'../')
from data_io import zbl_io
from tools.text_to_words import *


def append_file(fin, fout, fappend):
    """Copies all records from fin and fappend to fout.
    
    Returns number of all copied records."""
    counter = 0
    for record in zbl_io.read_zbl_records(fin):                        
        zbl_io.write_zbl_record(fout, record)
        fout.write("\n")    
        counter = counter + 1 
    for record in zbl_io.read_zbl_records(fappend):                    
        zbl_io.write_zbl_record(fout, record)
        fout.write("\n")    
        counter = counter + 1
    return counter


def copy_file(fin, fout):
    """Copies all records from fin to fout.
    
    Returns number of all copied records."""
    counter = 0
    for record in zbl_io.read_zbl_records(fin):                        
        zbl_io.write_zbl_record(fout, record)
        fout.write("\n")    
        counter = counter + 1 
    return counter

        



def filter_records(fin, fout, bad_ids_file):
    """Copies records from fin to fout. Filters out records of ids contained in file bad_ids_file (path).
    
    Returns list of skipped (filtered out) ids."""
    filter_ids = set(line.strip() for line in open(bad_ids_file).xreadlines())
    skipped_ids = set()        
    for record in zbl_io.read_zbl_records(fin):
        if record[zbl_io.ZBL_ID_FIELD] in filter_ids:
            skipped_ids.add(record[zbl_io.ZBL_ID_FIELD])
            continue
        zbl_io.write_zbl_record(fout, record)
        fout.write("\n")
    return skipped_ids


def keep_records_ids(fin, fout, keep_ids_file):
    """Copies records from fin to fout. Keeps only those records of ids contained in file keep_ids_file (path).
    
    Returns list of kept ids."""
    filter_ids = set(line.strip() for line in open(keep_ids_file).xreadlines())
    print len(filter_ids)," on the 'keep-ids' list"
    kept_ids = set()        
    for record in zbl_io.read_zbl_records(fin):
        if not record[zbl_io.ZBL_ID_FIELD] in filter_ids: continue
        kept_ids.add(record[zbl_io.ZBL_ID_FIELD])
        zbl_io.write_zbl_record(fout, record)
        fout.write("\n")
    return kept_ids


def filter_duplicates(fin, fout):
    """Copies records from fin to fout. Records with duplicated id are filtered out.
    
    Returns list of duplicated ids."""
    ids = set()
    duplicated_ids = set()
    for record in zbl_io.read_zbl_records(fin):
        id = fix_id(record[zbl_io.ZBL_ID_FIELD])        
        if id in ids:
            duplicated_ids.add(id)
            continue        
        ids.add(id)
        zbl_io.write_zbl_record(fout, record)
        fout.write("\n")    
    return duplicated_ids



###############################################################################


def copy_file_cmd(fin, fout, sys):
    print copy_file(fin, fout), "records copied."


def append_file_cmd(fin, fout, sys):
    append_file_path = sys.argv[4] 
    print append_file(fin, fout, open(append_file_path)), "records copied."


def filter_id_cmd_name(fin, fout, sys):
    """Removes records of id contained in id-list-file (single id per line)."""
    bad_ids_file = sys.argv[4]         
    skipped_ids = filter_records(fin, fout, bad_ids_file)
    print "Skipped ids:", skipped_ids
    print "Total:", len(skipped_ids)
    

def keep_id_cmd_name(fin, fout, sys):
    """Keeps records of id contained in id-list-file (single id per line)."""
    keep_ids_file = sys.argv[4]         
    kept_ids = keep_records_ids(fin, fout, keep_ids_file)
    print "Total kept:", len(kept_ids)


def filter_id_duplicates_cmd_name(fin, fout, sys):
    duplicated_ids = filter_duplicates(fin, fout)        
    print "Duplicated ids:", duplicated_ids
    print "Total:", len(duplicated_ids)
