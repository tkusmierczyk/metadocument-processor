
import tempfile

ZBL_ID_FIELD = 'an'
    
MULTIVAL_FIELD_SEPARATOR = '\t;\t'
MULTIVAL_FIELD_SEPARATOR2 = '\t,\t'
DICT_FIELD_SEPARATOR = ': '

BAD_MULTIVAL_FIELD_ENDINGS = ['\t;','\t,']

SEPARATOR = MULTIVAL_FIELD_SEPARATOR2
SEPARATOR_REPLACEMENT = '&'
EMPTY_VALUE = '?'


TEMPDIR = tempfile.gettempdir() #place to store temporary results

