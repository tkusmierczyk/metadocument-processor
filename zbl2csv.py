#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Reads ZBL file and stores CSV file."""

import sys
import logging
from data_io import zbl_io
from config import *
import argparse


def _format_value(value):
    try:
        if int(value)==float(value):
            return value
    except:
        pass
    try:
        v = float(value)
        return value
    except:
        pass
    return '"%s"' % value.replace("\"", "'")


def format_csv_line(values, separator=SEPARATOR, replacement=SEPARATOR_REPLACEMENT):
    return separator.join(_format_value(value).replace(separator, replacement) for value in values)


def order_record_generator(record, header, empty_value=EMPTY_VALUE):
    return (record.get(key, empty_value) for key in header)


if __name__ == "__main__":    

    reload(sys) 
    sys.setdefaultencoding('utf8')
    fin, fout = sys.stdin, sys.stdout
    sys.stdout = sys.stderr
    logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)

    print "The script reads ZBL file from stdin and stores CSV file to stdout."
    #print "Args [opt]: separator (for example: tab), comma-separated header, empty value marker."

    ###################################################################################################
    
    parser = argparse.ArgumentParser()
    parser.add_argument("-s", help="separator (for example: tab) [default: %s]" % SEPARATOR, 
                        nargs=1, metavar="", required=False, default=[SEPARATOR])
    parser.add_argument("-sr", help="separator replacement", 
                        nargs=1, metavar="", required=False, default=[" "])
    parser.add_argument("-t", help="comma-separated header", 
                        nargs=1, metavar="", required=False, default=[None])
    parser.add_argument("-e", help="empty value marker [default: %s]" % EMPTY_VALUE, 
                        nargs=1, metavar="", required=False, default=[EMPTY_VALUE])
    parser.add_argument("-k", help="key column name in the output file", 
                        nargs=1, metavar="", required=False, default=[ZBL_ID_FIELD])

    args = parser.parse_args()
    
    parser.set_defaults(feature=True)    
    
    
    # read arguments
    separator = args.s[0]
    if separator.lower()=="tab": separator="\t"

    separator_replacement = args.sr[0]

    try: header = args.t[0].split(',')
    except: header = None

    empty_value = args.e[0]

    key_column = args.k[0]

    ###################################################################################################

    print "separator = <%s>" % separator
    print "separator_replacement = <%s>" % separator_replacement
    print "header =",header
    print "empty_value = <%s>" % empty_value
    print "key_column = <%s>" % key_column

    ###################################################################################################

    if header is not None:
        fout.write("%s\n" % format_csv_line(header, separator))        

    for counter, record in enumerate( zbl_io.read_zbl_records(fin) ):   
        record[key_column] = record.pop(ZBL_ID_FIELD)                     
        if header is None:
            header = sorted(record.keys())
            header.remove(key_column) #force key column to be the first
            header = [key_column] + header 
            fout.write("%s\n" % format_csv_line(header, separator))
        csv_line = format_csv_line(order_record_generator(record, header, empty_value), separator, separator_replacement)
        fout.write("%s\n" % csv_line)
    print counter, "records stored"


