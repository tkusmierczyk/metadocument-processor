#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Measuring similarity between dictionaries of weights."""

import math

def dot(w, w2):
    return float( sum( v*w2.get(k,0.0) for k,v in w.iteritems() ) )

def cosine(w, w2):
    return dot(w, w2) / math.sqrt(dot(w, w)) / math.sqrt(dot(w2, w2))


