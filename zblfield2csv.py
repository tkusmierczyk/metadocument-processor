#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Reads ZBL file and stores CSV file."""

import sys
import logging
from data_io import zbl_io
from config import *


def format_csv_line(values, separator=SEPARATOR):
    return separator.join(value for value in values)


def order_record_generator(record, header, empty_value=EMPTY_VALUE):
    return (record.get(key, empty_value) for key in header)


if __name__ == "__main__":    

    reload(sys) 
    sys.setdefaultencoding('utf8')
    fin, fout = sys.stdin, sys.stdout
    sys.stdout = sys.stderr
    logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)

    print "The script reads ZBL file from stdin and stores CSV file to stdout."
    print "Args: dictionary {id:value} field name, separator [opt], empty value marker [opt]."

    try: 
        field_name = sys.argv[1].strip()
    except:
        print "Arg required: field name"
        sys.exit(-1)

    try: separator = "%s" % sys.argv[2]        
    except: separator = "\t"

    try: empty_value = sys.argv[3]
    except: empty_value = "0"


 
    print "field_name =",field_name
    print "separator =",separator
    print "empty_value =",empty_value


    print "reading"
    data = []
    header = set()
    missing = 0
    for counter, record in enumerate( zbl_io.read_zbl_records(fin) ): 
        if counter%10000==0: print "%i ..." % counter
        if field_name not in record:
            missing = 0
            data.append({})
        else:                       
            k2v = zbl_io.unpack_dictionary_field(record[field_name])
            key2vals = {}
            for k, v in k2v.iteritems():
                try:    key2vals[int(k)] = v
                except: key2vals[k] = v
            data.append(key2vals)
            header.update(key2vals.keys())
    header = sorted(header)
    print counter, "records loaded"
    if missing>0:
        print "WARN: %i does not have field %s" % (missing, field_name)
    print len(header),"header: ", str(header[:100])[:200]

    
    print "writing"
    fout.write("%s\n" % format_csv_line(map(unicode, header), separator))        
    for counter,record in enumerate(data):                
        if counter%10000==0: print "%i ..." % counter        
        fout.write("%s\n" % format_csv_line(order_record_generator(record, header, empty_value), separator))
    print counter, "records stored"

